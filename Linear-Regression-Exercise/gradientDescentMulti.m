function [theta, J_history, JvsIterations, theta_history] = gradientDescentMulti(X, y, theta, alpha, num_iters)
%GRADIENTDESCENTMULTI Performs gradient descent to learn theta
%   theta = GRADIENTDESCENTMULTI(x, y, theta, alpha, num_iters) updates theta by
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);
noOfFeatures = length(X(1, :));
JvsIterations = zeros(num_iters, 2);
theta_history = zeros(noOfFeatures, num_iters);
for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCostMulti) and gradient here.
    %

    predictions = X * theta;
    delta = zeros(noOfFeatures, 1);
    % for feature = 1:noOfFeatures
    %     delta(feature) = (1/m) * ( X(:, feature)' * (predictions - y) );
    % end

    % Vectorized approach
    delta = alpha * (1/m) * X'*(predictions-y)

    theta = theta - delta;
    theta_history(:, iter) = theta;







    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCostMulti(X, y, theta);
    JvsIterations(iter, 2) = J_history(iter);
    JvsIterations(iter, 1) = iter;
end

end

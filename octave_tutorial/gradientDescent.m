function y = gradientDescent(X, y, theta)
    a = 0.3 % learning rate
    m = size(X, 1) % # examples
    thetai = thetai - a * (1 / m * sum((thetai' * X - y) * X(i)))
end